import { Terminal } from './terminal.model';

class Terminals {
  Terminals?: Terminal;
}

export class StorageTerminalsData {
  IsSuccess?: boolean;
  Terminals?: Terminals[];
}

// export class TerminalsData {
//   BarData: BarData;
//   Charts: Charts;
//   IsSuccess: boolean;
//   Terminals: Terminal[];
// }


