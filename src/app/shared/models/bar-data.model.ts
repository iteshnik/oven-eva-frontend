

class BarData {
  Alarms: number;
  Events: number;
  Incaso: number;
  Sales: number;
}
export class StorageBarData {
  IsSuccess?: boolean;
  BarData?: BarData;
}
